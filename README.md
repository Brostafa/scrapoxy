## Setting up Scrapoxy on Google Compute Engine (5-10 minutes)
1- Create a VM (enable HTTP) and run `curl https://gist.githubusercontent.com/Brostafa/8b432ca6a4edcdb57a2cf9d6d521149e/raw/21c8e9f57b23e21bee439d97b7c5ff68cd32d67b/setup.sh --silent | bash`

2- Stop VM and take an image of it. Name the image `forward-proxy` (or any other name)

3- Grab you [Google Credentials](https://console.cloud.google.com/apis/credentials)
  - Select your project
  - Create Credentials -> Service account key
  - Select `Compute Engine` as the Service Account
  - Choose `JSON` as the Key type
  - Create

4- Update scrapoxy.json with the data you got from step 2 & 3

5- run `sudo pm2 start pm2.json5` and `sudo pm2 logs` to check everything is running smoothly

6- https://www.youtube.com/watch?v=dQw4w9WgXcQ


---
## Commands
`sudo pm2 stop 0` Stops scrapoxy

`sudo pm2 start 0` starts scrapoxy

`sudo pm2 reload 0` restarts scrapoxy

`sudo pm2 logs` opens logs



---
here's how `scarpoxy.json` should look like

```
{
  "commander": {
          "password": "password"
  },
  "instance": {
    "port": 3128,
    "scaling": {
        "min": 1,
        "max": 5
    }
  },
  "providers": [{
    "type": "googlecloud",
    "projectId": "psyched-silicon-205423",
    "credentials": {
        "client_email": "303072040006-compute@developer.gserviceaccount.com",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDOaxM/wjm3qt0t\n7aXo7LPOgGi5xi0v8SU808zDiSKG4FKOdomnuoYgc52ID83CavLmtbcckrV9BUsf\nlcgt0YXkMtML8FCardwdi6dbF3VUvhtuNj1QtQwE/jUYxH9gn/38Zw0QzjCMwwwS\njb/7gNTAI4AvRWTGBlUAsRopqlx8xBIfPyMIRWOH09ijOaHe0k89ts/sCSfl7llT\nEO2KqO1eZicKRIiClvJFKrCRj1UCYX9v/XKTn8itysc625qQy9XOrkDUBmeclK5Z\naaey/uwGlqkXnq/4DtIJv+r3J+GauqG4aNUQ1HzUrMJKHWzziVnQE2JpHRMU6P4L\nQe+XNB25AgMBAAECggEAJgSNnowgwkW7h/aBvLU/5rQ61aB8+RwCfkIKMCdlTw8o\n1WlasCRICNkSdhlnDbxR8ZEdYStaNuUPMYmOPXYeIWoE+elwhxSnel+a2smmyA1d\nkv51a5R02sSRNfcW+M5u4pp+Sw6W3Y4c/pvR9LoFNUA0n0A6L8jBxidYSL1CXI4R\nd9NtIb7TGC/hQvVg8PPH97DRjBWFGBF83xCjwKRy2ck6XxM0A74l6TGt0BS6PTb+\ndRLUkOK3zO9U3K3NqILSfVRAnNT4ES2jfXJnz/oOL/vCLLoY7TcZU/Zvzc77rVPC\nB2EVGLf3gl5uNEYaMuNlitPZ+lbX1Th/2kL/r2UZSwKBgQDyF2uwOFny9IxR6R/w\nRny0kCVnIPmJYwbCgotrSC0A96rcOiyUDtsBXaZUkbNHoaIX/AqVTWoiGnFfVBYK\ncFSzNOn3kBXSdbD6bzViICJcJ6/PGG3vv+TaaP2Y32gCg5mh7g+1/4hTvs06PBqi\nXj/A2yhMUqzsQckVewaAhS5COwKBgQDaRvzhcaMfEeVlAizeWVxnP1cUjd9Vb05Y\nwbT9CqtFpzx+FBu/1Zkv8Kf1RPpdBJc94eN+5AyunEZ7O6VD+xXcffJEmQGa0na3\n2OZrbFP+2DYogwoSz2ch4pMi3ceqfHHD3LvWgZCIWRMGU8KKQdxlK7wJ8Q0HbH/H\n7aDCfJHMmwKBgQDaspWyB/YaKRlqFbOol8SoSV/AnmVO24qLxOq16+nb6eEMOfFz\nf0gRH8zWsRjIKRgJmsLkxZ6PWFv6xjdzDq81hSOnZR6RlYhfdH9BOc5Qefm0mFkY\nML5JIHeX1ppbSw9K3YiL3WAGZ2N2rD1TWNk3G8R3zmgwte013oGtMKLtaQKBgHwj\nUz6D+ll5PBUuH4Z1G/A8PbIa3aEhJFWmFOC0aKO8cNLjbMS1HeH95AfBqwGr0rgG\n9/L6j5velNcRHPSEn/I+TsJkol0CxTHZ0XHWsg6FuIdhvyDjg6vpx/RUXXHARJM3\nvjxIg4KH5iH7EPDAdC+1p8Ofq+FE+4cgxgTWA7VxAoGASNYrhSEpcT3RhvIG8Ce5\nZzfoqoOkktltEQzVJ9ave42RVCntpfMOitLrpXrih9OH5c4shLC1GCcFp1++lLNv\nADpIzfVpzx5LWNHhxvWdPCTw4ME0WM8zd4lCVJjbn9FX1UijGkldXQUfpxGPCd/+\nQ8hg9KyNtapOEC1XdgMLGuk=\n-----END PRIVATE KEY-----\n"
    },
    "region": "us-central1-a",
    "imageName": "forward-proxy",
    "machineType": "n1-standard-1",
    "tags": "[\"forward-proxy\"]"
  }]
}
```

### Notes
- Don't name instance "proxy" because Scrapoxy uses proxy-<random_id> to identify its proxies, and having something that starts with "proxy" conflicts with it. It's okay naming it "scrapoxy" or even pproxy, anything as long as it doesn't start with "proxy".
- If you want to make scrapoxy picks random regions on Google Cloud then instead of specifying a region like `"region": "us-central1-a"` you can give it "random" `"region": "random"` 