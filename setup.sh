printf "\n=> Installing Node.js [Step: 1/4]\n"
sleep 2
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs build-essential

printf "\n=> Installing PM2 [Step: 2/4]\n"
sleep 2
sudo npm install pm2 -g

printf "\n=> Installing Scrapoxy and running initial scripts [Step: 3/4]\n"
sleep 2
sudo npm install https://gitlab.com/Brostafa/scrapoxy.git -g
curl --silent --location https://raw.githubusercontent.com/fabienvauchelles/scrapoxy/master/tools/install/proxy.js | sudo tee /root/proxy.js > /dev/null
curl --silent --location https://raw.githubusercontent.com/fabienvauchelles/scrapoxy/master/tools/install/proxyup.sh | sudo tee /etc/init.d/proxyup.sh > /dev/null
sudo chmod a+x /etc/init.d/proxyup.sh
sudo update-rc.d proxyup.sh defaults
sudo /etc/init.d/proxyup.sh start

printf "\n=> Creating sample files [Step: 4/4]\n"
sleep 2
pwd=`pwd`
echo "
{
  apps : [
    {
      name         : \"scrapoxy\",
      script       : \"/usr/bin/scrapoxy\",
      args         : [\"start\", \"$pwd/scrapoxy.json\", \"-d\"],
      kill_timeout : 30000,
    },
  ],
}
" > pm2.json5

echo "
{
  \"commander\": {
	  \"password\": \"password\"
  },
  \"instance\": {
    \"port\": 3128,
    \"scaling\": {
        \"min\": 1,
        \"max\": 5
    }
  },
  \"providers\": [{
    \"type\": \"googlecloud\",
    \"projectId\": \"PROJECT_NAME (could be: elated-liberty-202903)\",
    \"credentials\": {
        \"client_email\": \"CLIENT_EMAIL (could be: 982172029037-compute@developer.gserviceaccount.com)\",
        \"private_key\": \"PRIVATE_KEY\"
    },
    \"randomRegion\": \"true (could be: true/false - WITHOUT QUOTES)\" ,
    \"region\": \"REGION (could be: us-central1-a OR random)\",
    \"imageName\": \"IMAGE_NAME (could be: forward-proxy)\",
    \"machineType\": \"MACHINE_TYPE (could be: n1-standard-1 - https://cloud.google.com/compute/docs/machine-types)\",
    \"tags\": \"YOUR_TAGS (could be: [\\\"scrapoxy-server\\\"])\"
  }]
}
" > scrapoxy.json

printf "\n\n=> Script completed successfully!\n=> Please, fill in the missing values in scrapoxy.json then run 'sudo pm2 start pm2.json5'\n=> You will find both of these files in $pwd\n\n"